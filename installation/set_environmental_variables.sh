# Build the directories
TEMP_HMI_PATH=$(pwd)

# Removes previous version
sed -i '/HMI_PATH/d' ~/.bashrc

sleep 1

# Write them in the .bashrc file
echo export HMI_PATH="$TEMP_HMI_PATH" >> ~/.bashrc

echo "Set HMI_PATH as $TEMP_HMI_PATH"

