# Human Machine Interface 


The Human Machine Interface allows operators or users of the system to bi-directionally communicate with each aerial robotic agent. Both communication directions are equally important because:


* The communication from the robotic agent to the operator is needed to monitor its state and the performance of the requested mission. This will allow the operator to actuate in case of a need to redefine or abort the mission. 


* The communication from the operator to the robotic agent is needed if a non-rigid behavior is needed to accomplish the goal, making it possible to redefine the mission and also to start, stop and abort the mission.



## Developer’s Guide

###Obtaining source code and libraries
* Source code project:

        git clone https://german_quintero_rey@bitbucket.org/german_quintero_rey/human_machine_interface.git
        git checkout interface_dev


The Human Machine Interface depends on ROS and Qt.

* ROS framework: 
<http://wiki.ros.org/indigo/Installation/Ubuntu>

* Qt 4.8.6:
<http://download.qt.io/official_releases/qt/4.8/4.8.6/>

###Simple Installation 

```
cd ~/worskspace
git clone https://german_quintero_rey@bitbucket.org/german_quintero_rey/human_machine_interface.git
cd human_machine_interface/src
catkin_init_workspace
./installation/set_environmental_variables.sh
cd ..
catkin_make
```

###Installation in a catkin previous project

```
cd <my_worskspace>/src/<workspace_path>     => Example: cd ~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack/stack
git clone https://bitbucket.org/german_quintero_rey/human_machine_interface.git 
cd human_machine_interface
./installation/set_environmental_variables.sh
cd ~/<my_workspace>
catkin_make
```

This software has been tested on: Ubuntu 14.04 and open source drivers.


###Project organization

The project organization use a model/view architecture that manages the relationship between data and the way it is presented to the user.

```
Project /
    CMakeList.txt
    package.xml
    README.md
    src /             
        Controller /     
            source /
            include /
        View / 
            ui /
            resource /
              images.qrc
              images /
              osg_dataset /
            
    libraries /
    main.cpp
    
    test /
       Component1 /
                main.cpp
       Component2 /
                main.cpp
       ...
        
```

###Recommended IDEs

####Qt Creator
Qt Creator is a cross-platform C++, JavaScript and QML integrated development environment which is part of the SDK for the Qt GUI Application development framework.

Qt IDE: <http://www.qt.io/download/>

#####Installing Qt Creator 

To install Qt Creator, you can follow these steps:

* **Step 1.** Download Qt installer from its official page: http://www.qt.io/download/

* **Step 2.** Launch the Qt installer from the download folder in the terminal:

        user@host:~/Downloads$./qt-unified-linux-x64-2.0.1-online.run  
Default installer settings are enough to use Qt Creator, do not change if you do not have to.

* **Step 3.** Once Qt Creator is installed you should change the file project permissions. To do that, you can use this command in the terminal, just swap username to your username:

        sudo chown -R username /home/username/.config/QtProject/

* **Step 4.** The next step is to remove the soft link (CMakeLists.txt) stored in the src folder of your project and create a hard link of the same referred file. For instance, let us consider that there is a project stored at: ```/home/<username>/workspace/ros/quadrotor_stack_catkin/```  
The soft link CMakeLists.txt references to the file: ```/opt/ros/<ros_distribution>/share/catkin/cmake/toplevel.cmake```  
Create the hard link with these commands:

        cp CMakeLists.txt CMakeLists.txt.old
        rm CMakeLists.txt
        cp /opt/ros/<ros_distribution>/share/catkin/cmake/toplevel.cmake CMakeLists.txt


* **Step 5.** As we use catkin, we have to load the source files before opening Qt Creator the first time. In this example, we use these commands:

        source /opt/ros/<ros_distribution>/setup.bash
        source /<project_path>/devel/setup.bash

* **Step 6.** Launch Qt Creator with this command:

        ~/Qt/Tools/QtCreator/bin/qtcreator.sh

* **Step 7.** Once Qt Creator is running, we have to select “open a project” and look up the file CMakeLists.txt that was changed in the project. Then we push next button and, finally, run CMake button.




For more information:
<http://wiki.ros.org/IDEs>
