#include "../include/mission_reporter.h"
#include "ui_mission_reporter.h"

MissionReporter::MissionReporter(QWidget *parent, MissionStateReceiver *mission_receiver) :
    QWidget(parent),
    ui(new Ui::MissionReporter)
{
    ui->setupUi(this);

    mission_name = "";
    mission_in_progress=false;
    index=1;
    mission_planner_receiver = mission_receiver;
    emergency_land_flag=false;
    current_action_arguments="";
    setSignalHandlers(); // triggers



    //ui->report->setText("No mission has been loaded. \n");


}

MissionReporter::~MissionReporter()
{
    delete ui;
}

/*!********************************************************************************************************************
 *  \brief      This method establishes the appropiate connections between elements (buttons, textEdit, ROS nodes...)
 *
 *********************************************************************************************************************/
void MissionReporter::setSignalHandlers()
{
    //connect(ui->commands,SIGNAL(activated(const QString &)),this,SLOT(setParameterLayout(const QString &)));
    //connect(ui->send,SIGNAL(clicked()),this,SLOT(sendCommand()));
    //connect(ui->managepoints,SIGNAL(clicked()),this,SLOT(managePoints()));
    connect(mission_planner_receiver,SIGNAL(missionLoaded(QString)),this,SLOT(displayMissionLoaded(const QString)));
    connect(mission_planner_receiver,SIGNAL(taskReceived(QString)),this,SLOT(displayCurrentTask(const QString)));
    connect(mission_planner_receiver,SIGNAL(missionCompleted(bool)),this,SLOT(displayMissionCompleted(const bool)));
    connect(mission_planner_receiver,SIGNAL(actionReceived(QString)),this,SLOT(displayCurrentAction(const QString)));
    connect(mission_planner_receiver,SIGNAL(missionStarted()),this,SLOT(displayMissionStart()));
    connect(mission_planner_receiver,SIGNAL(emergencyLand()),this,SLOT(emergencyLandReceived()));
    connect(mission_planner_receiver,SIGNAL(actionCompleted(bool)),this,SLOT(displayActionCompleted(const bool)));
    connect(mission_planner_receiver,SIGNAL(actionArguments(QString)),this,SLOT(actionArgumentsReceived(const QString)));

}

void MissionReporter::displayMissionLoaded(const QString &mission)
{


    if (!mission_in_progress)
    {
        mission_name=mission;
        //QString text="The mission '"+mission_name+"' has been loaded.\n\n";
        //ui->report->setText(text);
    }

}

void MissionReporter::displayMissionStart()
{
    emergency_land_flag=false;
    current_action_arguments="";
    mission_in_progress=true;
    QString text=QString::number(index)+". "+"Started mission: "+mission_name;
    ui->report->append(text);
    index++;
}

void MissionReporter::displayCurrentTask(const QString &current_task)
{
    QString text=QString::number(index)+". "+"Task: "+current_task;
    ui->report->append(text);
    index++;
}

void MissionReporter::displayCurrentAction(const QString &current_action)
{
    if (mission_planner_receiver->is_autonomous_mode_active && !emergency_land_flag)
    {
        QString text=QString::number(index)+". "+"Action: "+current_action+" "+current_action_arguments;
        ui->report->append(text);
        index++;
        current_action_arguments="";
    }
}

void MissionReporter::displayMissionCompleted(const bool mission_completed)
{
    mission_in_progress=false;
    QString text;
    if (mission_completed)
    {
        text=QString::number(index)+". "+"Completed mission: "+mission_name+"\n";
    }

    else
    {
        text=QString::number(index)+". "+"Aborted mission: "+mission_name+"\n";
    }
    ui->report->append(text);
    index=1;
}

void MissionReporter::displayActionCompleted(const bool action_completed)
{
    QString text;
    if (!action_completed)
    {
        text=QString::number(index)+". "+"Action failure.";
        ui->report->append(text);
        index++;
    }


}

void MissionReporter::emergencyLandReceived(){
    emergency_land_flag=true;
}

void MissionReporter::actionArgumentsReceived(const QString action_arguments){

    current_action_arguments=action_arguments;



}

