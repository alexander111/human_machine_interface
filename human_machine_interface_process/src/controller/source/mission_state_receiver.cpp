/*
  MissionStateReceiver
  Launch a ROS node to subscribe topics.
  @author  Yolanda de la Hoz Simón, Germán Quintero Rey
  @date    04-2016
  @version 2.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include "../include/mission_state_receiver.h"


/*****************************************************************************
** Implementation
*****************************************************************************/

MissionStateReceiver::MissionStateReceiver(){
     subscriptions_complete = false;
}


void MissionStateReceiver::openSubscriptions(ros::NodeHandle n, std::string rosnamespace){
    // Topic communications

    if (!n.getParam("drone_Current_Task_Topic_Name", current_task_topic))
        current_task_topic = "current_task";

    if (!n.getParam("drone_Approved_Action_Topic_Name", current_action_topic))
        current_action_topic = "approved_action";

    if (!n.getParam("drone_Completed_Mission_Topic_Name",completed_mission_topic))
        completed_mission_topic = "completed_mission";

    if (!n.getParam("drone_Completed_Action_Topic_Name",completed_action_topic))
        completed_action_topic = "completed_action";

     current_task_subs=n.subscribe(current_task_topic, 1, &MissionStateReceiver::currentTaskCallback, this);
     current_action_subs=n.subscribe(current_action_topic, 1, &MissionStateReceiver::currentActionCallback, this);
     completed_mission_subs=n.subscribe(completed_mission_topic,1,&MissionStateReceiver::completedMissionCallback,this);
     completed_action_subs=n.subscribe(completed_action_topic,1,&MissionStateReceiver::completedActionCallback,this);

     subscriptions_complete = true;
     is_autonomous_mode_active=false;
//    real_time=ros;

      //load_mission_client=n.serviceClient<droneMsgsROS::openMissionFile>("/" + rosnamespace +  "/" + "open_mission_file");
      mission_name_client=n.serviceClient<droneMsgsROS::missionName>("/" + rosnamespace +  "/" + "mission_name");
      start_mission_client=n.serviceClient<std_srvs::Empty>("/" + rosnamespace +  "/" + "task_based_mission_planner_process/start");
      stop_mission_client=n.serviceClient<std_srvs::Empty>("/" + rosnamespace +  "/" + "task_based_mission_planner_process/stop");

}


bool MissionStateReceiver::ready() {
    if (!subscriptions_complete)
        return false;
    return true; //Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}


MissionStateReceiver::~MissionStateReceiver() {}

void MissionStateReceiver::activateAutonomousMode(){
     if(!is_autonomous_mode_active){
     //std::cout<<"Starting Mission Planner...."<<std::endl;
     //std_srvs::Empty emptySrvMsg;
     //mission_planner_srv_start.call(emptySrvMsg);
     is_autonomous_mode_active=true;
     requestMissionName();
     }

}

void MissionStateReceiver::deactivateAutonomousMode(){
     if(is_autonomous_mode_active){
     //std_srvs::Empty emptySrvMsg;
     //mission_planner_srv_stop.call(emptySrvMsg);
     is_autonomous_mode_active=false;
     }
 }

void MissionStateReceiver::currentActionCallback(const droneMsgsROS::actionData::ConstPtr &msg)
{
       current_action=*msg;
       std::vector<droneMsgsROS::actionArguments> action_arguments;
       QString arguments;

       if (current_action.ack==true) {
          QString accion;
           switch(current_action.mpAction){
           case droneMsgsROS::actionData::TAKE_OFF:
               accion="TAKE OFF";
               break;
           case droneMsgsROS::actionData::HOVER:
                accion="WAIT";
                action_arguments=current_action.arguments;
                arguments="("+QString::number(action_arguments.at(0).value.at(0))+")";
                Q_EMIT actionArguments(arguments);
               break;
           case droneMsgsROS::actionData::LAND:
                accion="LAND";
               break;
           case droneMsgsROS::actionData::STABILIZE:
               accion="STABILIZE";
               break;
           case droneMsgsROS::actionData::MOVE:
                accion="MOVE";
               break;
           case droneMsgsROS::actionData::GO_TO_POINT:
                accion="GO TO POINT";
                action_arguments=current_action.arguments;
                arguments="("+QString::number(action_arguments.at(0).value.at(0))+", "+QString::number(action_arguments.at(0).value.at(1))+", "+QString::number(action_arguments.at(0).value.at(2))+")";
                Q_EMIT actionArguments(arguments);
               break;
           case droneMsgsROS::actionData::ROTATE_YAW:
                accion="ROTATE YAW";
                action_arguments=current_action.arguments;
                arguments="("+QString::number(action_arguments.at(0).value.at(0))+")";
                Q_EMIT actionArguments(arguments);
               break;
           case droneMsgsROS::actionData::FLIP:
                accion="FLIP";
               break;
           case droneMsgsROS::actionData::FLIP_RIGHT:
                accion="FLIP RIGHT";
               break;
           case droneMsgsROS::actionData::FLIP_LEFT:
                accion="FLIP LEFT";
               break;
           case droneMsgsROS::actionData::FLIP_FRONT:
                accion="FLIP FRONT";
               break;
           case droneMsgsROS::actionData::FLIP_BACK:
                accion="FLIP BACK";
               break;
           case droneMsgsROS::actionData::UNKNOWN:
                accion="UNKNOWN";
               break;
           }


           Q_EMIT actionReceived(accion);



       }
    }


void MissionStateReceiver::currentTaskCallback(const std_msgs::String::ConstPtr &msg)
{

       task_name=msg->data;

       Q_EMIT taskReceived(QString::fromStdString(task_name));



}

void MissionStateReceiver::completedMissionCallback(const std_msgs::Bool::ConstPtr &msg)
{

       bool completed_mission=msg->data;

       Q_EMIT missionCompleted(completed_mission);



}

void MissionStateReceiver::completedActionCallback(const droneMsgsROS::CompletedAction::ConstPtr &msg)
{

       droneMsgsROS::CompletedAction completed_action=*msg;

       if(completed_action.final_state==droneMsgsROS::CompletedAction::SUCCESSFUL)
           Q_EMIT actionCompleted(true);
       else
           Q_EMIT actionCompleted(false);




}

bool MissionStateReceiver::requestMissionName()
{

    droneMsgsROS::missionName msg;
    if(mission_name_client.call(msg))
    {
        Q_EMIT missionLoaded(QString::fromStdString(msg.response.mission_name));
        return true;
    }

    else
    {

      return false;
      /*ERROR no se pudo llamar al servicio"*/
    }

}

bool MissionStateReceiver::loadMission(const std::string file_path)
{

  droneMsgsROS::openMissionFile loadSrvMsg;
  loadSrvMsg.request.mission_file_path = file_path;

  if(load_mission_client.call(loadSrvMsg))
  {
    if (loadSrvMsg.response.ack)
      {
      //Show dialog. Mission specification file opened successfully.

      // Update mission name.


      Q_EMIT missionLoaded(QString::fromStdString(loadSrvMsg.response.mission_name));
      return true;
      }

      else
      {
      //Show dialog. Detected errors in mission specification file.



      Q_EMIT missionErrors(loadSrvMsg.response.error_messages);
      return true;

      }
  }


  else
  {
    //ERROR no se pudo llamar al servicio
    return false;
  }







}

bool MissionStateReceiver::startMission()
{
    std_srvs::Empty emptySrvMsg;
    if(start_mission_client.call(emptySrvMsg))
    {
        Q_EMIT missionStarted();
        return true;
    }

    else
    {
      return false;
      /*ERROR no se pudo llamar al servicio"*/
    }

}

bool MissionStateReceiver::abortMission()
{
    std_srvs::Empty emptySrvMsg;
    if(stop_mission_client.call(emptySrvMsg))
    {
        Q_EMIT missionCompleted(false);
        return true;
    }

    else
    {
      return false;
      /*ERROR no se pudo llamar al servicio"*/
    }

}

void MissionStateReceiver::emitEmergencyLand()
{
    Q_EMIT emergencyLand();
    activateAutonomousMode();
}

/*droneMsgsROS::actionData MissionStateReceiver::getMissionInfo(){

    return mission_info;
}*/


void MissionStateReceiver::log( const LogLevel &level, const std::string &msg) {
	logging_model.insertRows(logging_model.rowCount(),1);
	std::stringstream logging_model_msg;
	switch ( level ) {
		case(Debug) : {
				ROS_DEBUG_STREAM(msg);
				logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Info) : {
				ROS_INFO_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Warn) : {
				ROS_WARN_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Error) : {
				ROS_ERROR_STREAM(msg);
				logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Fatal) : {
				ROS_FATAL_STREAM(msg);
				logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
				break;
		}
	}
	QVariant new_row(QString(logging_model_msg.str().c_str()));
	logging_model.setData(logging_model.index(logging_model.rowCount()-1),new_row);
	Q_EMIT loggingUpdated(); // used to readjust the scrollbar
}


