/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/behaviour_viewer.h"
#include "ui_behaviourviewer.h"
#include <qt4/Qt/qdebug.h>
#include <qt4/Qt/qscrollbar.h>
#include <string>
#include <sstream>
#include <qt4/Qt/qmenu.h>
#include <qt4/Qt/qtablewidget.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

/*****************************************************************************
** Implementation
*****************************************************************************/

BehaviourViewer::BehaviourViewer(QWidget *parent, RosGraphReceiver *collector, UserCommander *usercommander) :
    QWidget(parent),
    ui(new Ui::BehaviourViewer)
{
    ui->setupUi(this);
    ui->table_process_viewer->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    behavior_receiver=collector;
    //connect(behavior_receiver,SIGNAL(stateBehaviorReceived(droneMsgsROS::behaviorsList* list_behavior_state)),this,SLOT(onBehaviourStateReceived(droneMsgsROS::BehaviorsList *list_behavior_state)));
    connect(ui->table_process_viewer,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(onCustomContextMenuRequested(const QPoint&)));
    connect(behavior_receiver,SIGNAL(stateSkill()),this,SLOT(onSkillStateReceived()));

    user_command=usercommander;

    initializeBehaviourViewerTable();


}

void BehaviourViewer::clearFocus()
{
    //std::cout<<"Editing line finished" <<std::endl;
    //ui->line_edit->clearFocus();
}


void BehaviourViewer::initializeBehaviourViewerTable(){
    //Inicializar tabla de behaviours. Solicitar lista a behaviour manager? QUITAR
}


void BehaviourViewer::updateBehaviourState(const droneMsgsROS::SkillDescriptor *behavior_container, int row_behavior_viewer)
{
    //ui->table_process_viewer->setItem(row_behavior_viewer,1,new QTableWidgetItem("Not Requested"));
    switch(behavior_container->current_state.state)
    {
    case droneMsgsROS::SkillState::not_requested:
        ui->table_process_viewer->setItem(row_behavior_viewer,1,new QTableWidgetItem("Not Requested"));
        break;

    case droneMsgsROS::SkillState::requested:
        ui->table_process_viewer->setItem(row_behavior_viewer,1,new QTableWidgetItem("Requested"));
        break;

     }
}

void BehaviourViewer::onSkillStateReceived()
{
  int row_behavior_viewer=0;
  //std::cout << "Loop the list to create the items in the table"   << std::endl;
  for(unsigned int i = 0; i < behavior_receiver->skill_list.skill_list.size(); i++)
  {
      behavior_containter= behavior_receiver->skill_list.skill_list.at(i);

      if(!initialized_table){
          if (ui->table_process_viewer->rowCount() < row_behavior_viewer)
              ui->table_process_viewer->setRowCount(row_behavior_viewer);
          ui->table_process_viewer->insertRow(row_behavior_viewer);
      }
      ui->table_process_viewer->setItem(row_behavior_viewer,0,new QTableWidgetItem(QString(behavior_containter.name.c_str())));

      updateBehaviourState(&behavior_containter,row_behavior_viewer);
      row_behavior_viewer++;
  }
  initialized_table=true;
}

void BehaviourViewer::onTextFilterChange(const QString &arg1)
{

}

void BehaviourViewer::onCustomContextMenuRequested(const QPoint& pos) {
    if(initialized_table){
        QTableWidgetItem* item = ui->table_process_viewer->itemAt(pos);
        if(ui->table_process_viewer->column(item)==0){
            if (item) {
                // Map the point to global from the viewport to account for the header.
                showContextMenu(item,  ui->table_process_viewer->viewport()->mapToGlobal(pos));
            }
        }
    }

}

void BehaviourViewer::showContextMenu(QTableWidgetItem* item, const QPoint& globalPos){
    QMenu menu;
    //menu.addAction("    ");
    menu.addAction("Request");
    menu.addAction("Do not request");
    //menu.addAction("Record(Not implemented)");
    //connect(&menu, SIGNAL(triggered(QAction*)), this, SLOT(menuSelection(QAction*)));
    //menu.exec(globalPos);
}

void BehaviourViewer::menuSelection(QAction* action){

   std::cout<<"Action clicked:" + action->text().toStdString() <<std::endl;

    QModelIndexList selection = ui->table_process_viewer->selectionModel()->selectedRows();

    // Multiple rows can be selected
    for(int i=0; i< selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        QTableWidgetItem *item = ui->table_process_viewer->item( index.row(), 0 );
        std::cout<< item->text().toStdString() <<std::endl;
        if(action->text().toStdString().compare("Stop")==0)
         user_command->processMonitorCommander(item->text().toStdString(),processMonitorStates::Stop);
        if(action->text().toStdString().compare("Start")==0)
         user_command->processMonitorCommander(item->text().toStdString(),processMonitorStates::Start);
        if(action->text().toStdString().compare("Reset")==0)
         user_command->processMonitorCommander(item->text().toStdString(),processMonitorStates::Reset);
    }
}



BehaviourViewer::~BehaviourViewer()
{
    delete ui;
}

