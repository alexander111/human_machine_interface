/*!*******************************************************************************************
 *  \file       mission_state_receiver.h
 *  \brief      MissionStateReceiver definition file.
 *  \details    This file includes the MissionStateReceiver class declaration. To obtain more
 *              information about it's definition consult the mission_state_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/

#ifndef HumanMachineInterface_MISSIONSTATERECEIVER_H_
#define HumanMachineInterface_MISSIONSTATERECEIVER_H_

#include <ros/ros.h>
#include <string>
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "droneMsgsROS/droneMissionInfo.h"
#include <qt4/Qt/qstring.h>
#include <qt4/QtCore/QtDebug>
#include <qt4/Qt/qstringlistmodel.h>
#include "droneMsgsROS/actionData.h"
#include "droneMsgsROS/actionArguments.h"
#include "droneMsgsROS/openMissionFile.h"
#include "droneMsgsROS/missionName.h"
#include "droneMsgsROS/CompletedAction.h"


class MissionStateReceiver: public QObject {
    Q_OBJECT
public:
         MissionStateReceiver();
	virtual ~ MissionStateReceiver();

        void run();
        bool ready();

	enum LogLevel {
        Debug,
        Info,
        Warn,
        Error,
        Fatal
	};

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);
    void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);
    bool is_autonomous_mode_active;
    void activateAutonomousMode();
    void isInAutonomousMode();
    void deactivateAutonomousMode();
    //droneMsgsROS::actionData getMissionInfo();
  
  bool startMission();
  bool abortMission();
  bool requestMissionName();
  void emitEmergencyLand();

  
  bool loadMission(const std::string file_path); 
  
  

    Q_SIGNALS:
        void loggingUpdated();
        void rosShutdown();
        void actionReceived(const QString action);
	void taskReceived(const QString task);
	void missionLoaded(const QString mission);
	void missionErrors(const std::vector<std::string> error_messages);
	void missionCompleted(const bool ack);
	void missionStarted();
	void actionCompleted(const bool ack);
	void emergencyLand();
	void actionArguments(const QString action_arguments);


	
private:
  std::string current_task_topic;
  std::string current_action_topic;
  std::string completed_mission_topic;
  std::string completed_action_topic;
  
  droneMsgsROS::actionData current_action;
  std::string task_name;

    int init_argc;
    int real_time;
    char** init_argv;
    bool subscriptions_complete;
    
  ros::ServiceClient load_mission_client;
  ros::ServiceClient mission_name_client;
  
  ros::ServiceClient start_mission_client;
  ros::ServiceClient stop_mission_client;
  
    ros::Subscriber current_task_subs;
  ros::Subscriber current_action_subs;
  ros::Subscriber completed_mission_subs;
  ros::Subscriber completed_action_subs;
  
    void currentActionCallback(const droneMsgsROS::actionData::ConstPtr &msg);
  void currentTaskCallback(const std_msgs::String::ConstPtr &msg);
  void completedMissionCallback(const std_msgs::Bool::ConstPtr &msg);
  void completedActionCallback(const droneMsgsROS::CompletedAction::ConstPtr &msg);
  
  
    QStringListModel logging_model;
};
#endif /* HumanMachineInterface_mission_state_receiver_h_ */
