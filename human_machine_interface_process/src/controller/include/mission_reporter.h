#ifndef MISSION_REPORTER_H
#define MISSION_REPORTER_H

#include <QWidget>
#include <QString>

#include "mission_state_receiver.h"

namespace Ui {
class MissionReporter;
}

class MissionReporter : public QWidget
{
    Q_OBJECT

public:
    explicit MissionReporter(QWidget *parent = 0, MissionStateReceiver *mission_receiver=0);
    ~MissionReporter();

  void setSignalHandlers();



public Q_SLOTS:
  //void setParameterLayout(const QString &command);
  void displayMissionLoaded(const QString &mission_name);
  void displayCurrentTask(const QString &current_task);
  void displayCurrentAction(const QString &current_action);
  void displayMissionCompleted(const bool mission_completed);
  void displayMissionStart();
  void displayActionCompleted(const bool action_completed);
  void emergencyLandReceived();
  void actionArgumentsReceived(const QString action_arguments);

private:

    QString mission_name;
  bool mission_in_progress;
  QString current_action_arguments;
  int index;
  bool emergency_land_flag;
    Ui::MissionReporter *ui;
    MissionStateReceiver *mission_planner_receiver;
};

#endif // MISSION_REPORTER_H
